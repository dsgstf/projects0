# STF-P 
## _The DSG Software Task Force Proposal/Project_

The DSG Software Task Force operates under the premise of improving student, staff, and faculty life on campus, and providing a way for students to build large-scale, maintained applications. This provides a great opportunity for new and interesting ideas to be put to practice, but we have limited resources to do so. To ease in our project management, the DSG-P form can be used to submit proposals for anything that you think DSGSTF should be doing.
  
### Submission
 
Simply email the document to dsgstf@duke.edu. It will be processed and added to the DSGSTF [project repository](https://gitlab.oit.duke.edu/dsgstf/projects), where you can track any changes or progress. If the proposal is accepted, it will be updated with additional project details

### Structure

All submissions must follow the below structure. The goal is to make these documents small, easy to read, but able to describe fully the required efforts of implementation. Focus on brevity. The proposals may be edited briefly for appearance without note and to provide a standard appearance for all proposals.
 
 - Title
 - 5 Sentence summary
 - Rationale
 - Full Proposal
 - Required Resources
 - Responsibilities (Description of who will be responsible for what in the long term)
 - Optional Appendices: Implementation Plans, Timeline

These sections are fairly self-explanatory. They also should fit on only a handful of pages. If supporting data or figures are required, include them in additional appendices. A proposal (without appendicies) should be 1-2 pages total, and readable in 10 minutes or less. (like this document!)

### If Accepted

DSG-P's that are accepted are referred to as projects, and are given extra information during the approval process. Portions of the original proposal may also be modified at this point. The information below (in single-line list format) is prepended to the proposal and moved into the folder `in_progress`:
 
 - Status Summary
 - Team
 - Estimated Time to Complete
 - Time Consumed
 - Resources utilized
 - Roadblocks

Once completed, project files will be moved to the `maintenance` folder.

### If Rejected
 
 Any rejected proposals will be moved to the `rejected` folder, and prepended with the following information:
 
 - Rationale for Rejection
 - Relevant Advice
 